Тестовое задание на SDK Corona (https://coronalabs.com/)
Язык программирования Lua

Задание:
Пропарсить файл формата JSON "images.json" и по полученным ссылкам вывести на экран изображения с возможностью пролистывать их (widget.ScrollView). 
Реализовать это на сцене (использовать composer). Файлы во вложении:

{
	"images":[
		"http://www.google.com/intl/ru_ALL/analytics/images/row-img-stats.png",
		"http://www.google.com/intl/ru_ALL/analytics/images/social_hero.png",
		"http://www.google.com/analytics/images/social_promo_conversion.png",
		"http://www.google.com/analytics/images/social_promo_conversion.png",
		"http://www.google.com/analytics/images/social_promo_referral.png",
		"http://www.google.com/analytics/images/social_promo_plugins.png",
		"http://www.google.com/analytics/images/socialconversions_hero.png",
		"http://www.google.com/analytics/images/socialreferrals_hero.png",
		"http://www.google.com/analytics/images/socialplugins_hero.png",
		"http://www.google.com/intl/ru_ALL/analytics/images/feat-hed-mobile.png"
	]
}