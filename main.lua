local composer = require("composer");

-- значения по умолчанию
contentW = display.contentWidth - 20; -- ширина контента
contentH = display.contentHeight - 20; -- высота контента

-- Настройки цвета фона и текста
------------------------------------------------------------------

display.setDefault("background", 47/255, 79/255, 79/255); -- фон приложения
display.setDefault("fillColor", 0); -- цвет текста по умолчанию

-- Переходим к сцене gallery
composer.gotoScene("scenes.gallery");
