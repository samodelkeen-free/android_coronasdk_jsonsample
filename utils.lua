-- Библиотека служебных функций
local json = require("json");

local utils = {}
local files = {}

-- Загрузка данных из файла
function utils.loadJson(filename)
	local path = system.pathForFile(filename, system.ResourceDirectory);
	local contents = "";
	local myTable = {};
	local file = io.open(path, "r");
	if file then
		 local contents = file:read( "*a" );
		 myTable = json.decode(contents);
		 io.close(file);
		 print ( "JSON count: ", table.maxn(myTable.images) )
		 return myTable;
	end
	return nil
end

-- Обработчик HTTP-сессии
-- Так не вышло
--[[
local function networkListener( event )
	if ( event.isError ) then
		print( "Network error - download failed: ", event.response )
	elseif ( event.phase == "began" ) then
		print( "Progress Phase: began" )
	elseif ( event.phase == "ended" ) then
		-- Отключаем отображение, т.к. нужна только загрузка:
		--[
		print( "Displaying response image file" )
		myImage = display.newImage( event.response.filename, event.response.baseDirectory, 60, 40 )
		myImage.alpha = 0
		transition.to( myImage, { alpha=1.0 } )
		]
		--[
		print ( "event.response.fullPath: ", event.response.fullPath )
		print ( "event.response.filename: ", event.response.filename )
		print ( "event.response.baseDirectory: ", event.response.baseDirectory )
		]
		table.insert(files, event.response.filename);
	end
end
]]

-- Масштабирование картинки
function utils.fitImage( displayObject, fitWidth, fitHeight, enlarge )
	--
	-- first determine which edge is out of bounds
	--
	local scaleFactor = fitHeight / displayObject.height
	local newWidth = displayObject.width * scaleFactor
	if newWidth > fitWidth then
		scaleFactor = fitWidth / displayObject.width 
	end
	if not enlarge and scaleFactor > 1 then
		return 1
	end
	displayObject:scale( scaleFactor, scaleFactor )
	return scaleFactor
end

function utils.placeImage( view, filename, currentY )
	image = display.newImage( filename, system.DocumentsDirectory );
	image.x = display.contentCenterX;
	scaleFactor = utils.fitImage( image, contentW - 20, contentW - 20, true );
	image.y = currentY + image.height * scaleFactor / 2;
	currentY = currentY + image.height * scaleFactor + 5;
	view:insert( image );
	return currentY;
end 

-- Очищает таблицу загруженных файлов
function utils.clearFiles()
	for i = #files, 1, -1 do
		table.remove(files, i)
	end
end

-- Проверка существования файла
function utils.fileExists(fileName, base)
	assert(fileName, "fileName does not exist")
	local base = base or system.ResourceDirectory
	local filePath = system.pathForFile( fileName, base )
	local exists = false
 
	if (filePath) then
		local fileHandle = io.open( filePath, "r" )
		if (fileHandle) then
			exists = true
			io.close(fileHandle)
		end
	end
 
	return(exists)
end

-- Таймер задержки (для тестирования)
function utils.sleep(n)  -- seconds
	local t0 = os.clock()
	while os.clock() - t0 <= n do end
end

utils.files = files;

return utils;