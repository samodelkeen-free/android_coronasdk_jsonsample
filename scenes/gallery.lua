local composer = require("composer");
local widget = require("widget");
local utils = require("utils");
local scene = composer.newScene();

function scene:create(event)
	local sceneGroup = self.view;
	local currentY = 0; --Текущая позиция картинки в скролле

	-- создаём группу для скроллера и сопутствующих контролов
	local scrollGroup = display.newGroup();
	
	-- ScrollView listener
	local function scrollListener( event ) -- Пока нигде не используем

		local phase = event.phase
		--[[
		if ( phase == "began" ) then print( "Scroll view was touched" )
		elseif ( phase == "moved" ) then print( "Scroll view was moved" )
		elseif ( phase == "ended" ) then print( "Scroll view was released" )
		end
		]]
		-- In the event a scroll limit is reached...
		if ( event.limitReached ) then
		--[[
			if ( event.direction == "up" ) then print( "Reached bottom limit" )
			elseif ( event.direction == "down" ) then print( "Reached top limit" )
			elseif ( event.direction == "left" ) then print( "Reached right limit" )
			elseif ( event.direction == "right" ) then print( "Reached left limit" )
			end
		]]
		end

		return true
	end

	-- создаем скроллер
	local scrollView = widget.newScrollView(
		{
			parent = scrollGroup,
			top = 10,
			left = 0,
			width = contentW,
			height = contentH,
			scrollWidth = contentW,
			scrollHeight = contentH,
			verticalScrollDisabled = false,
			horizontalScrollDisabled = true,
			hideBackground = true,
			listener = scrollListener
		}
	)

	-- Обработчик HTTP-сессии
	local function networkListener( event )
		if ( event.isError ) then
			print( "Network error - download failed: ", event.response )
		elseif ( event.phase == "began" ) then
			print( "Progress Phase: began" )
		elseif ( event.phase == "ended" ) then
			-- Отключаем отображение, т.к. нужна только загрузка:
			--[[
			print( "Displaying response image file" )
			myImage = display.newImage( event.response.filename, event.response.baseDirectory, 60, 40 )
			myImage.alpha = 0
			transition.to( myImage, { alpha=1.0 } )
			]]
			--[[
			print ( "event.response.fullPath: ", event.response.fullPath )
			print ( "event.response.filename: ", event.response.filename )
			print ( "event.response.baseDirectory: ", event.response.baseDirectory )
			]]
			currentY = utils.placeImage( scrollView, event.response.filename, currentY );
		end
	end
	
	-- Заполняем таблицу картинок из файлов или загружаем из интернета, вставляя в ScrollView (порядок картинок не соблюдается, т.к. еще не разобрался с асинхронностью!)
	utils.clearFiles();
	jsonData = utils.loadJson("data/images.json"); -- загружаем адреса картинок из файла JSON
	if (jsonData.images) then
		local params = {}
		params.progress = true
		for i = 1, #jsonData.images do
			urlname = jsonData.images[i];
			filename = urlname:match( "([^/]+)$" );
			-- Проверяем, если файл уже был загружен, то не загружаем его из интернетов:
			if ( not utils.fileExists(filename, system.DocumentsDirectory) ) then
				network.download( urlname, "GET", networkListener, params, filename, system.DocumentsDirectory ); -- загружаем картинки из интернетов
			else
				table.insert(utils.files, filename);
			end
		end
	end

	--utils.sleep(3);
	print ( "files count: ", table.maxn(utils.files) );
	if ( table.maxn(utils.files) > 0 ) then
		for i = 1, #utils.files do
			currentY = utils.placeImage( scrollView, utils.files[i], currentY );
		end
	end

	-- Вставляем все имеющиеся на сцене объекты в sceneGroup
	sceneGroup:insert(scrollGroup);
end

scene:addEventListener("create", scene);
return scene;
